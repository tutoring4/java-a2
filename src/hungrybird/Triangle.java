package hungrybird;

public class Triangle {
    private double side1;
    private double side2;
    private double side3;

    public Triangle() {
        this.side1 = 3.0;
        this.side2 = 4.0;
        this.side3 = 5.0;
    }

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double getParameter() {
        return this.side1 + this.side2 + this.side3;
    }

    public double getArea() {
        double helper = this.getArea() / 2;
        double inside = (helper / 2) * (helper - this.side1) * (helper - this.side1) * (helper - this.side1);
        return Math.sqrt(inside);
    }


    @Override
    public String toString() {
        return String.format("Triangle: side1 = %f, side2 = %f, and side3 = %f\n", this.side1, this.side2, this.side3);
    }
}

