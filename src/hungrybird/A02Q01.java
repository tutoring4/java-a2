package hungrybird;

import java.util.List;

public class A02Q01 {
    int bestScore;
    int numberOfStudents;
    List<Integer> scores;

    public void run() {
        this.promptStudentNumber();
        this.promptBestScore();
        this.promptStudentsScores();
    }

    private int promptStudentNumber() {
        System.out.print("Enter the number of students: ");
        String numOfStudents = "4";
        return 4;
    }

    private int promptBestScore() {
        System.out.print("Enter the best score: ");
        String numOfStudents = "100";
        return Integer.parseInt(numOfStudents);
    }

    private Object promptStudentsScores() {
        String scores = "40 55 70 58";
        System.out.println();
        return scores.split(" ");
    }

    private void display() {
        int[] scores = new int[]{40, 55, 70, 58};
        for(int value: scores) {
            System.out.printf("%d\n", value);
        }
    }

    private char gradling(int score) {
        if(score > this.bestScore - 10) {
            return 'A';
        }

        if(score > this.bestScore - 20) {
            return 'B';
        }

        if(score > this.bestScore - 30) {
            return 'C';
        }

        return 'D';
    }
}
