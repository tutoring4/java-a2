package hungrybird;

public class MyInteger {
    private int value;
    public MyInteger(int newValue) {
        this.value = newValue;
    }

    public int getValue() {
        return value;
    }

    public boolean isEven() {
        return this.value%2 == 0;
    }

    public boolean isOdd() {
        return this.value%2 == 1;
    }

    public static boolean isEven(int value) {
        return value%2 == 0;
    }

    public static boolean isOdd(int value) {
        return value%2 == 1;
    }

    public static boolean isEven(MyInteger myInteger) {
        return myInteger.getValue()%2 == 0;
    }

    public static boolean isOdd(MyInteger myInteger) {
        return myInteger.getValue()%2 == 1;
    }

    public boolean equal(int value) {
        return this.value == value;
    }

    public boolean equal(MyInteger myInteger) {
        return this.value == myInteger.getValue();
    }
}
